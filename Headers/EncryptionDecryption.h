/*************************************************************
Author: Group 5
Date: 19/06/2015
Company: Vimarshana
Desc: Header files containing the function declarations of 
encryption and decryption logic
*************************************************************/
#ifndef ENCRYPTIONDECRYPTION_H_
#define ENCRYPTIONDECRYPTION_H_
void read_terminal(void);
void read_file(void);
void encrypt_line(char *e);
void decrypt(int,char []);
void get_filename(void);
void want_to_save(void);
void printencrypted(int,int []);//to print the character uses length and index as its parameter
extern char *LookUpTable,ToEncrypt[100],ch[10],dec[100];
extern char c1[30],d1[30];
extern int length,i,j,z,l,k[100],DecKey[100]; 
extern char *DigitLookupTable[];
extern char *AlphabetLowerCaseLookupTable[],*AlphabetUpperCaseLookupTable[];
extern char DigitPosition[],AlphabetLowerCasePosition[],AlphabetUpperCasePosition[];
extern char *FileName;
#endif

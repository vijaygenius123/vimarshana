/*************************************************************
Author: Group 5
Date: 19/06/2015
Company: Vimarshana
Desc: Header files containing the function declarations of 
encryption and decryption logic
*************************************************************/

int read_line();
void encrypt_line(char *e);
void decrypt(int,int []);
void printencrypted(int,int []);//to print the character uses length and index as its parameter
char c1[30]="abcdefghijklmnopqrstuvwxyz";  /*string initialization used to find index value in alphabet*/  
char d[10]="0123456789";  /*string initialization used to find index value for digit*/  
char *b,c2[100],ch[10],dec[100];  
int c=0,i,j,z,l=0,k[100],dec_key[100]; //encryption pattern inialization, 3 encryption pattern for digits
char *DigitLookupTable[]={"5678901234","8901234567","2345678901"};

//encryption pattern inialization, 10 encryption pattern for text
char *LowerLookupTable[]={"pqrstuvwxyzabcdefghijklmno","vwxyzabcdefghijklmnopqrstu","ghijklmnopqrstuvwxyzabcdef","klmnopqrstuvwxyzabcdefghij","opqrstuvwxyzabcdefghijklmn","tuvwxyzabcdefghijklmnopqrs","ijklmnopqrstuvwxyzabcdefgh","defghijklmnopqrstuvwxyzabc","xyzabcdefghijklmnopqrstuvw","rstuvwxyzabcdefghijklmnopq"};

char *UpperLookupTable[]={"RSTUVWXYZABCDEFGHIJKLMNOPQ",
"XYZABCDEFGHIJKLMNOPQRSTUVW",
"DEFGHIJKLMNOPQRSTUVWXYZABC",
"IJKLMNOPQRSTUVWXYZABCDEFGH",
"TUVWXYZABCDEFGHIJKLMNOPQRS",
"OPQRSTUVWXYZABCDEFGHIJKLMN",
"KLMNOPQRSTUVWXYZABCDEFGHIJ",
"GHIJKLMNOPQRSTUVWXYZABCDEF",
"VWXYZABCDEFGHIJKLMNOPQRSTU",
"PQRSTUVWXYZABCDEFGHIJKLMNO",
};

char *SymbolLookupTable[]={

":;<=>?@[\\]^_`{|}~!\"#$%&\'()*+,-./",
"{|}~!\"#$%&\'()*+,-./:;<=>?@[\\]^_`"
"!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~",
};


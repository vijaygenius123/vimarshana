/***************************************************

Author: Group 5
Date: 22/06/2015
Desc: Header file for Menu

*****************************************************/

#ifndef MENU_H_
#define MENU_H_

extern int Choice,SubChoiceEnc,SubChoiceDec;


void display_menu(void);
void display_submenu_encryption(void);
void display_submenu_decryption(void);


#endif

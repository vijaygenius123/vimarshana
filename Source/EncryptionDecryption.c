/*************************************************************
Author: Group 5
Date: 19/06/2015
Company: Vimarshana
Desc: length file containing the function body of 
encryption and decryption logic
*************************************************************/
#include <stdio.h>
#include <string.h>
#include "EncryptionDecryption.h"
void read_terminal()
{
    int CharacterCount=0;
    char c;
    do
    {
        c = fgetc(stdin);
         if(c == EOF)
        break;
        ToEncrypt[CharacterCount] = c;
        CharacterCount++;
    }
    while(1);
    length = CharacterCount;
    puts(ToEncrypt);
}
/***********************************************************
Prototype:
Desc:
Parameters:
*************************************************************/


void read_file(void)
{         
        int CharacterCount;
        int FileLen,i=0;
        char c;
        char FileName[50];
        FILE *fp;
        printf("Enter File Name:");
        scanf("%s",FileName);
        fp = fopen(FileName,"r");
        if(fp == NULL)
         {
            printf("Error Opening File");
         }
        else
        {
        while(!feof(fp))
        {
            c = fgetc(fp);
            if(c != EOF)
                {
                    ToEncrypt[i] = c;
                    
                }
                i++;
        }
        }
        puts(ToEncrypt);
        fclose(fp);
        length = i;

}

void want_to_save(void)
{
 int CharacterCount;
        int FileLen,i=0;
        char c;
        char FileName[50];
        FILE *fp;
        printf("Enter File Name For Saving:");
        scanf("%s",FileName);
        fp = fopen(FileName,"w");
        for(i=0;i<length;i++)
        {
        fputc(dec[i],fp);
        }
        printf("\n File Written Sucessfully \n");
        fclose(fp);
}

void encrypt_line(char e[100])
{
	for(j=0;j<length;j++)	  
    	{ 
		if(isdigit(e[j]))
		{	
			for(i=0;i<10;i++)
		        {
                    		if(e[j]==DigitPosition[i])    
		        	{			
                        		k[j]=i;
                    		}
			}		
		}
		if(e[j]==' ')
		{
			k[j]=32;
			
		}
		
		else if(e[j]=='\n')
		{
			k[j]=10;
			
		}
		
		else if(isalpha(e[j]))
		{		//To find position value in english alphabet
		        if(islower(e[j]))
		        {
                	for(i=0;i<26;i++)
		             {
                    		if(e[j]==AlphabetLowerCasePosition[i])    
		                 	{			
                        		k[j]=i;
                    		}
			         }
			    }
			    else
			    {
			        for(i=0;i<26;i++)
		             {
                    		if(e[j]==AlphabetUpperCasePosition[i])    
		                 	{			
                        		k[j]=i;
                    		}
			         }
			    
			    }
			    
		}	
		
	}	
}
void printencrypted(int m,int enc[]) 
{     
	int z=0,j=0,i=0;
	for(l=0;l<m;l++)
	{
		if(enc[l]==32)
		{
			printf("%c",32);
			dec[l] = 32;
			
		}
		
		if(enc[l]==10)
		{
			printf("%c",10);
			dec[l] = 10;
			
		}
		
		else if((isalpha(ToEncrypt[l])))
		{
			z=z%10;	
			if(islower(ToEncrypt[l]))		    
    			LookUpTable = AlphabetLowerCaseLookupTable[z];
    		else
    		    LookUpTable = AlphabetUpperCaseLookupTable[z];
			printf("%c",LookUpTable[enc[l]]);
			dec[l] = LookUpTable[enc[l]];
			z++;
		}
		else
		{
			if(isdigit(ToEncrypt[l]))
			{
				if(j>2)
		        	{
                    			j=j%3;		
				}
				LookUpTable=DigitLookupTable[j];
				printf("%c",LookUpTable[enc[l]]);
				dec[l] = LookUpTable[enc[l]];
				j++;
			}
		}
	}
	printf("\n");
}

void decrypt(int m,char k[])
{	
	printf("Decrypted:\n");
	int z=0,j=0,i=0,l=0;
	for(l=0;l<m;l++)
	{
		if(dec[l]==32)
		{
			printf("%c",32);
		}
		else if(dec[l]==33)
		{
			printf("%c",33);
		}
		
		else if(dec[l]==10)
		{
		printf("%c",10);
		}
		
		else if((isalpha(dec[l])))
		{
			z=z%10;	
			if(islower(ToEncrypt[l]))
			    {		    
    			    LookUpTable = AlphabetLowerCaseLookupTable[z];
    	    		for(i=0;i<26;i++)
		            {
                    		if(LookUpTable[i]==dec[l])    
		        	        {			
                        		DecKey[l]=i;
					            printf("%c",AlphabetLowerCasePosition[i]);
                    		}
			        }
			     }   
    		else
    		    {
    		        LookUpTable = AlphabetUpperCaseLookupTable[z];
    		        for(i=0;i<26;i++)
		                 {
                    		if(LookUpTable[i]==dec[l])    
		        	        {			
                        		DecKey[l]=i;
					            printf("%c",AlphabetUpperCasePosition[i]);
                    		}
			             }
			    }
			
			z++;
		}
		else if(isdigit(dec[l]))
			{
				if(j>2)
		        	{
                    			j=j%3;		
				}
				LookUpTable=DigitLookupTable[j];
					for(i=0;i<10;i++)
		       				 {
		                    		if(LookUpTable[i]==dec[l])    
					        	{			
                        					DecKey[l]=i;
								printf("%c",DigitPosition[i]);	
                    					}
						    }   
				            j++;
			}
}       
  	printf("\n");
}

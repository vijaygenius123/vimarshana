/************************************************************
Author: Group 5
Date : 20/06/2015
Company: Vimarshana
Desc: .Program of 10 pattern subtitution method for a given text of line 
 ************************************************************/ 
#include<stdio.h>  //Header files
#include<string.h>  // String Header File
#include<malloc.h>  // Memory Allocation Header File
#include<stdlib.h>
#include"EncryptionDecryption.h"
#include"Menu.h"
///////////////// Global Declarations ////////////////////
char AlphabetLowerCasePosition[30]="abcdefghijklmnopqrstuvwxyz";  
char AlphabetUpperCasePosition[30]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
char DigitPosition[10]="0123456789";  
char *DigitLookupTable[]={"5678901234","8901234567","2345678901"};
char *AlphabetLowerCaseLookupTable[]={"pqrstuvwxyzabcdefghijklmno","vwxyzabcdefghijklmnopqrstu","ghijklmnopqrstuvwxyzabcdef","klmnopqrstuvwxyzabcdefghij","opqrstuvwxyzabcdefghijklmn","tuvwxyzabcdefghijklmnopqrs","ijklmnopqrstuvwxyzabcdefgh","defghijklmnopqrstuvwxyzabc","xyzabcdefghijklmnopqrstuvw","rstuvwxyzabcdefghijklmnopq"};
char *AlphabetUpperCaseLookupTable[]={"PQRSTUVWXYZABCDEFGHIJKLMNO","VWXYZABCDEFGHIJKLMNOPQRSTU","GHIJKLMNOPQRSTUVWXYZABCDEF","KLMNOPQRSTUVWXYZABCDEFGHIJ","OPQRSTUVWXYZABCDEFGHIJKLMN","TUVWXYZABCDEFGHIJKLMNOPQRS","IJKLMNOPQRSTUVWXYZABCDEFGH","DEFGHIKLMNOPQRSTUVWXYZABC","XYZABCDEFGHIJKLMNOPQRSTUVWXYZ","RSTUVWXYZABCDEFGHIJKLMNOP"};
int  length=0,l=0;
int  i,j,z,k[100],DecKey[100]; 
char *LookUpTable,ToEncrypt[100]="",ch[10],dec[100];
int Choice,SubChoiceEnc=0,SubChoiceDec=0;
char* FileName;
FILE *fp;
long Filelen=0;
/////////////////////  Main Starts Here ////////////////////

int main(void)
{
	/*	
	    int m;
	    printf("Enter text:\n"); 	
        do
	    {

    	read_terminal();
    	encrypt_line(ToEncrypt);
		printencrypted(length,k);
		
		 
		decrypt(length,dec);
    	printf("PRESS 0 IF THE WORDS ARE COMPLETED  OTHERWISE TYPE THE NEXT LINE\n");
    	scanf("%d",&m);
		length=0;
    	}
    	while(m!=0);
    	*/
        do
        {
        display_menu();            
        switch(Choice)
                {
                case 1:display_submenu_encryption();
                       switch(SubChoiceEnc)
                       {
                       case 1: printf("Enter Data To Be Encrypted\n");
                               read_terminal();
                               length = strlen(ToEncrypt);
                               encrypt_line(ToEncrypt);
                               printf("Encrypted Data:\n");
                               printencrypted(length,k);
                               want_to_save();
                               break;
                       case 2: read_file();
                               printf("File Read\n");
                               encrypt_line(ToEncrypt);
                               printf("Encrypted Data:\n");
                               printencrypted(length,k);
                               want_to_save();
                               break;
                       default: printf("Invalid Input , Try Again");
                                display_submenu_encryption();
                       }
                       break;
              case 2:display_submenu_decryption();
                      switch(SubChoiceDec)
                      {
                      case 1:read_terminal();
                             decrypt(length,ToEncrypt);
                             break;
                      case 2:read_file();
                             printf("File Read\n");
                             decrypt(length,ToEncrypt);
                             want_to_save();
                             break;
                      case 3:decrypt(length,ToEncrypt);
                             break;
                      default: printf("Invalid Input , Try Again");
                               display_submenu_decryption();
                      }
//                        decrypt(length,dec);
                      break;
              case 3:printf("Thank You");
                     exit(-1);
                    break;
              default: printf("Invalid Input , Try Again");
                       display_menu();
                       break;
              }           
        }
        while(Choice<=3);
        
}
